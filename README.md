# nfs_client role

Makes a host an NFS client to access file systems epxorted by an NFS server.

## Supported Operating Systems

* Ubuntu
  * >= 16.04

* Fedora

## Variables

### Global variables

This is information that is not specific to IPA but rather general
information about the host or the network. This information's natural
residence is in the inventory but could be provided through other
means.

Information about these variables is found in the root level README.md
file.

* `network_domain`

## Dependencies

The nfs_server role depends on this role (nfs_client), so there is no
need to apply both roles.
